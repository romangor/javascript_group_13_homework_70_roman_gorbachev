import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormComponent } from './form/form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { PhoneValidatorDirective } from './directives/phone-validator.directive';
import { HttpClientModule } from '@angular/common/http';
import { ApplicationService } from './shared/application.service';
import { SubmissionComponent } from './submission.component';

@NgModule({
  declarations: [
    AppComponent,
    FormComponent,
    PhoneValidatorDirective,
    SubmissionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [ApplicationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
