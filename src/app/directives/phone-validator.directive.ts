import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator, ValidatorFn } from '@angular/forms';
import { Directive } from '@angular/core';

@Directive ({
  selector: '[appPhone]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: PhoneValidatorDirective,
    multi: true
  }]
})

export class PhoneValidatorDirective implements Validator {
  validate(control: AbstractControl): ValidationErrors | null {
    return phoneValidator()(control);
  }
}

export function phoneValidator(): ValidatorFn {
  return  (control: AbstractControl): ValidationErrors | null => {
    const phone = (/(\+996)[\s]*\d{3}[\s]*\d{2}\d{2}\d{2}/).test(control.value);
    if (phone) {
      return null;
    }
    return  {number: true};
  }
}

