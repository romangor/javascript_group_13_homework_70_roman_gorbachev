import { Application } from "./application.model";
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';


@Injectable()
export class ApplicationService {
  sending = new Subject<boolean>()

  constructor(private http: HttpClient,
              private router: Router){}

  sendApplication(application: Application){
    this.sending.next(true);
    const body = {
      firstName: application.firstName,
      lastName: application.lastName,
      middleName: application.middleName,
      phoneNumber: application.phone,
      placeOfWork: application.placeOfWork,
      gender: application.gender,
      size: application.size,
      skills:application.skills,
      comment: application.comment,
    }
    this.http.post('https://projectsattractor-default-rtdb.firebaseio.com/applications.json', body).subscribe(() => {
      this.sending.next(false);
      void this.router.navigate(['/submission']);
    });
  }
}
