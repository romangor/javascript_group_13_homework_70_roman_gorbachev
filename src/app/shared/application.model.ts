export class Application {
  constructor(
    public firstName: string,
    public lastName: string,
    public middleName: string,
    public phone: string,
    public placeOfWork: string,
    public gender: string,
    public size: string,
    public skills: [{}],
    public comment: string
  ) {
  }
}
