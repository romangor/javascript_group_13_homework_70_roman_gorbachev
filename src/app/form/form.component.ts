import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { phoneValidator } from '../directives/phone-validator.directive';
import { ApplicationService } from '../shared/application.service';
import { Application } from '../shared/application.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit, OnDestroy {
  form!: FormGroup;
  sending!: boolean;
  sendSubscribe!: Subscription;

  constructor(private applicationService: ApplicationService) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      middleName: new FormControl('', Validators.required),
      phone: new FormControl('', [
        Validators.required,
        phoneValidator
      ]),
      placeOfWork: new FormControl('', Validators.required),
      gender: new FormControl('', Validators.required),
      size: new FormControl('', Validators.required),
      skills: new FormArray([]),
      comment: new FormControl('', Validators.required),
    })
    this.sendSubscribe = this.applicationService.sending.subscribe( (isSending: boolean) => {
      this.sending = isSending;
    })
  }

  getSkillsControls() {
    const skills = <FormArray>(this.form.get('skills'));
    return skills.controls;
  }

  addSkill() {
    const skills = <FormArray>this.form.get('skills');
    const skillGroup = new FormGroup({
      skill: new FormControl('', Validators.required),
      level: new FormControl('', Validators.required),
    })
    skills.push(skillGroup);
  }

  fieldErrors(fieldName: string, errorType: string){
    const field = this.form.get(fieldName);
    return Boolean(field && field.touched && field.errors?.[errorType]);
  }


  onSend() {
    const application =  new Application(
      this.form.value.firstName,
      this.form.value.lastName,
      this.form.value.middleName,
      this.form.value.phone,
      this.form.value.placeOfWork,
      this.form.value.gender,
      this.form.value.size,
      this.form.value.skills,
      this.form.value.comment);
    this.applicationService.sendApplication(application);
  }

  ngOnDestroy() {
    this.sendSubscribe.unsubscribe();
  }
}
