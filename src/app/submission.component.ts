import { Component } from '@angular/core';

@Component({
  selector: 'successful',
  template: `<h1>Thank you for your application</h1>`,
  styles: [`
    h1 {
      color: rgba(216, 47, 17, 0.54);
    }
  `]
})
export class SubmissionComponent {}
